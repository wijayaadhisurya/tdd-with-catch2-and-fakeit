#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"
#include <chrono>
#include <memory>

using namespace std;

//
//Cache is read through:
//If cache hit, return data from cache
//        If cache miss, read data from storage, add to cache, return data
//
//        Cache is write back:
//
//Write data to cache
//After some period P, batch write dirty cache to database

class Storage {
public:
    virtual ~Storage(){};

    virtual void write(int key, int value) = 0;
    virtual int read(int key) = 0;
};

class Timer {
public:
    virtual ~Timer() = default;
    virtual void set_period(chrono::seconds period) = 0;
    virtual void set_callback(std::function<void(chrono::time_point<chrono::steady_clock>)> callback) = 0;
};

class Cache {
public:
    virtual ~Cache() = default;

    virtual void write(int key, int value) = 0;
    virtual int read(int key) = 0;
};

class WriteBackCache : public Cache {
public:
    virtual void set_write_back_duration(chrono::seconds duration) = 0;
};

class SimpleCache : public WriteBackCache {
public:
    SimpleCache(std::shared_ptr<Storage> storage, std::shared_ptr<Timer> timer) {
        m_storage = storage;
        m_timer = timer;
    }

    void write(int key, int value) override {
        m_items[key] = value;
        m_dirty_write_keys.insert(key);
    }

    int read(int key) override {
        if (auto it = m_items.find(key); it != end(m_items)) {
            return it->second;
        } else {
            auto item = m_storage->read(key);
            m_items[key] = item;
        }

        return 0;
    }

    virtual void set_write_back_duration(chrono::seconds duration) {
        m_timer->set_period(duration);
        m_timer->set_callback([duration, this](chrono::time_point<chrono::steady_clock> now) {
            if (now - m_last_saved > duration) {
                for (auto key : m_dirty_write_keys) { m_storage->write(key, m_items[key]); }

                m_dirty_write_keys.clear();
                m_last_saved = now;
            }
        });
    }

private:
    shared_ptr<Storage> m_storage;
    map<int, int> m_items;
    shared_ptr<Timer> m_timer;
    set<int> m_dirty_write_keys;
    chrono::time_point<chrono::steady_clock> m_last_saved;
};


SCENARIO("Read through, write-back caching Policy without fakeit") {
    class MockStorage1 : public Storage {
    public:
        virtual ~MockStorage1() {}

        void write(int key, int value) {
            m_numWriteCalled++;

            m_items[key] = value;
        }
        int read(int key) {
            m_numReadCalled++;

            return m_items[key];
        }

        int get_num_read_called() { return m_numReadCalled; }

        int get_num_write_called() { return m_numWriteCalled; }

        void reset() {
            m_numWriteCalled = 0;
            m_numReadCalled = 0;
        }

    private:
        int m_numReadCalled = 0;
        int m_numWriteCalled = 0;
        std::map<int, int> m_items;
    };

    class MockTimer : public Timer {
    public:
        virtual ~MockTimer() = default;
        void set_period(chrono::seconds period) override { m_period = period; }

        void set_callback(std::function<void(chrono::time_point<chrono::steady_clock>)> callback) override {
            m_callback = callback;
        }

        void raise_timeout(chrono::seconds duration) {
            now += duration;
            m_callback(now);
        }

    private:
        chrono::time_point<chrono::steady_clock> now;
        chrono::seconds m_period = 1s;
        std::function<void(chrono::time_point<chrono::steady_clock>)> m_callback;
    };

    GIVEN("Cache empty") {
        auto timer = make_shared<MockTimer>();
        auto mockStorage = make_shared<MockStorage1>();
        SimpleCache cache{mockStorage, timer};
        cache.read(1);
        cache.read(2);
        cache.read(3);

        WHEN("Cache read") {
            THEN("Get item from storage, add cache") { REQUIRE(mockStorage->get_num_read_called() == 3); }
        }
    }

    GIVEN("Cache populated with some items") {
        auto mock_timer = make_shared<MockTimer>();
        auto mock_storage = make_shared<MockStorage1>();
        SimpleCache cache{mock_storage, mock_timer};
        cache.read(1);
        cache.read(3);
        cache.read(7);
        mock_storage->reset();

        WHEN("Cache hit") {
            THEN("Return item from cache") {
                cache.read(1);
                cache.read(3);
                cache.read(7);
                REQUIRE(0 == mock_storage->get_num_read_called());
            }
        }

        WHEN("Write to cache") {
            cache.write(27, 212);
            cache.write(28, 313);
            cache.write(29, 414);

            THEN("read items, read from cache") {
                cache.read(27);
                cache.read(28);
                cache.read(29);

                REQUIRE(0 == mock_storage->get_num_write_called());
                REQUIRE(0 == mock_storage->get_num_read_called());
            }
        }

        WHEN("Time to save") {
            cache.set_write_back_duration(20s);
            cache.write(1, 100);
            cache.write(3, 300);
            mock_timer->raise_timeout(7s);
            cache.write(8, 800);
            mock_timer->raise_timeout(7s);
            mock_timer->raise_timeout(7s);

            THEN("write to storage") { REQUIRE(3 == mock_storage->get_num_write_called()); }
        }
    }
}


#include "fakeit.hpp"

using namespace fakeit;

SCENARIO("Read through, write-back caching Policy with FakeIt") {

    Mock<Storage> mock_storage;
    When(Method(mock_storage, read)).AlwaysReturn(19);
    When(Method(mock_storage, write)).AlwaysReturn();
    shared_ptr<Storage> p_mock_storage = shared_ptr<Storage>(&mock_storage(), [](...) {});
    Mock<Timer> mock_timer;
    shared_ptr<Timer> p_mock_timer = shared_ptr<Timer>(&mock_timer(), [](...) {});

    SimpleCache cache{p_mock_storage, p_mock_timer};

    GIVEN("Cache empty") {
        cache.read(1);
        cache.read(2);
        cache.read(3);

        WHEN("Cache read") {
            THEN("Get item from storage, add cache") { Verify(Method(mock_storage, read)).Exactly(3_Times); }
        }
    }

    GIVEN("Cache populated with some items") {
        cache.read(1);
        cache.read(3);
        cache.read(7);
        mock_storage.ClearInvocationHistory();

        WHEN("Cache hit") {
            THEN("Return item from cache") {
                cache.read(1);
                cache.read(3);
                cache.read(7);

                Verify(Method(mock_storage, read)).Never();
            }
        }

        WHEN("Write to cache") {
            cache.write(27, 212);
            cache.write(28, 313);
            cache.write(29, 414);

            THEN("read items, read from cache") {
                cache.read(27);
                cache.read(28);
                cache.read(29);

                Verify(Method(mock_storage, read)).Never();
                Verify(Method(mock_storage, write)).Never();
            }
        }

        WHEN("time to save") {
            std::function<void(chrono::time_point<chrono::steady_clock>)> mock_timer_callback;
            When(Method(mock_timer, set_period)).AlwaysReturn();
            When(Method(mock_timer, set_callback)).AlwaysDo([&mock_timer_callback](auto cb) {
              mock_timer_callback = cb;
            });

            chrono::time_point<chrono::steady_clock> now;

            cache.set_write_back_duration(20s);
            cache.write(1, 100);
            cache.write(3, 300);
            mock_timer_callback(now + 7s);
            cache.write(8, 800);
            mock_timer_callback(now + 14s);
            mock_timer_callback(now + 21s);

            THEN("write to storage") { Verify(Method(mock_storage, write)).Exactly(3_Times); }
        }
    }
}
