class: center, middle

# C++ TDD With Catch2 and FakeIt

## Testing Your C++ Code Have Never Been Easier

### (by Wijaya Adhisurya)

---

## About Me

- Long time grumpy (embedded) C++ programmer
- Fear undefined behaviors, memory leaks, and segmentation fault a lot
- Modern C++ makes me grumpier
- But I won't go back to C++ 98 or plain ANSI C
- Also regularly code Java and C#
- Can read some Python, Go, and Rust

---

## Where I Work

![logo-hariff](logo-hariff.png)

- PT. Hariff Daya Tunggal Engineering
- An Indonesian defense contractor
- Products: Battlefield Management System, Network Monitoring System
- Defense Research Area: Military Information Management, Tactical Mobile Ad Hoc Network, Command and Control
- Technologies Used: Android, Java, .NET, Microcontrollers, SBC (Mostly ARM)

---

## Why TDD Matters

- Discover Bug Early
- Automated
- Repeatable
- Feedback Loop
- It's Faster! (Believe Me)

---

## How We Should Do It

- Plan your test scenarios
- Treat tests as production code
- Avoid duplicate tests
- Avoid combination tests
- Identify internal states and plan for it
- Test the interface, not the implementation

---

## Common TDD Myths

- It's the same as unit testing
- TDD leads to bad design
- TDD is a QA Strategy (No, it's a development method)
- It slows development down

---

## Where TDD Becomes Impractical (1)

- Legacy Code (can't put test easily)
- Embedded Systems (expensive device to setup, can't test HW drivers)
- Multithreaded components (race condition)
- Time-senstive scenarios (must wait too long)

---

## About Catch2

- Testing Framework for C++
- Supports both classical TDD and BDD
- Avoid code duplication via clever macros
- The most delicious testing framework I ever use (well, compared to JUnit, XUnit.NET, or NUnit)
- It's not AAA (Arrange, Act, Assert), but I think it's better

---

## Catch2 Example

```cpp
SCENARIO( "vectors can be sized and resized", "[vector]" ) {

    GIVEN( "A vector with some items" ) {
        std::vector<int> v( 5 );

        REQUIRE( v.size() == 5 );
        REQUIRE( v.capacity() >= 5 );

        WHEN( "the size is increased" ) {
            v.resize( 10 );

            THEN( "the size and capacity change" ) {
                REQUIRE( v.size() == 10 );
                REQUIRE( v.capacity() >= 10 );
            }
        }
        WHEN( "the size is reduced" ) {
            v.resize( 0 );

            THEN( "the size changes but not capacity" ) {
                REQUIRE( v.size() == 0 );
                REQUIRE( v.capacity() >= 5 );
            }
        }
    }
}
```

---

## About FakeIt

- The only other library that's not Google Mock when I search "C++ Mock"
- Feels familiar to me who always liked Moq (and somewhat like Mockito)
- Single header file
- Pretty much limited, but it's okay (can't use -O2 or -O3 in GCC)

---

## FakeIt Example

Declaring some interface

```cpp
struct SomeInterface {
	virtual int foo(int) = 0;
	virtual int bar(string) = 0;
};
```

Make a mock from it

```cpp
Mock<SomeInterface> mock;

When(Method(mock,foo)).Return(0);

SomeInterface &i = mock.get();

// Production code
i.foo(1);

// Verify method mock.foo was invoked.
Verify(Method(mock,foo));

// Verify method mock.foo was invoked with specific arguments.
Verify(Method(mock,foo).Using(1));
```

---

## Case Study: A Caching Policy

Requirements:

Cache is read through:

1. If cache hit, returns data from cache
2. If cache miss, reads data from storage, add item to cache

Cache is write back:

1. Write data to cache
2. After some period P, batch write dirty cache items to storage